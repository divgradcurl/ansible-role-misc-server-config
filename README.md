misc-server-config
=========

Miscellaneous configuration of my servers.

1. Install packages from `package_list`
2. Set up persistent journald storage
3. Install `dnf-automatic` and sets up automatic updates
4. Set time zone
5. Set up system-wide vimrc.
6. TODO: Ensure firewall is configured

Requirements
------------

A working email server is required if `dnf_automatic_emit_via` is set to `email`.

Role Variables
--------------

```
package_list
```
A list of extra packages to install.
Currently:
  * htop
  * tmux

```
timezone
```
Default: `UTC`.
You can get a list of timezones by running `timedatectl list-timezones`.

```
dnf_automatic_emit_via
```
Default: `stdio`. This configures how notifications will be sent: either through stdio or email.

```
dnf_automatic_email_from
```
Default: `root@example.com`. If `dnf_automatic_emit_via` is set to `email`, set this to which email will send DNF automatic notifications.

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: mailserver
      become: yes
      vars:
        timezone: America/New_York
        dnf_automatic_emit_via: email
        dnf_automatic_email_from: "DNF Automatic <root@dexample.com>"
      roles:
        - name: misc-server-config

License
-------

BSD

Author Information
------------------

June 2020 - G. F. AbuAkel
