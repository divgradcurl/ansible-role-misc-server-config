" Vim "guerrilla" config
" These are simple extra configs that make Vim more enjoyable
set number
set laststatus=2
"set ruler " this is set earlier
set shiftwidth=2
set tabstop=2
set expandtab
set smarttab
set autoindent
set scrolloff=0
set mouse=a
inoremap jk <ESC>

" for solarized8 colorscheme
" installation:
" git clone https://github.com/lifepillar/vim-solarized8.git ~/.vim/pack/themes/opt/solarized8
" OR for system-wide config:
" git clone https://github.com/lifepillar/vim-solarized8.git \
" /usr/share/vim/vim80/pack/dist/opt/solarized8
set termguicolors
colorscheme solarized8
if strftime("%H") >= 23 || strftime("%H") <= 6
  set background=dark
else
  set background=light
endif
